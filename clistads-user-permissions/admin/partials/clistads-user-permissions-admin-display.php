<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
