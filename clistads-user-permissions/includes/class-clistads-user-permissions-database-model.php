<?php

class Clistads_User_Permissions_Database_Model
{

    public function Create_New_User()
    {
        global $wpdb;
        $table = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_TABLE_NAME;
        $wpdb->query("INSERT INTO $table VALUES(NULL)");
        return $wpdb->insert_id;
    }
    public function Insert_Ip_Address(string $ipAddress, int $uniqueUserId)
    {
        global $wpdb;
        $table = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_IP_ADDRESS_TABLE_NAME;
        $data = array('user_id' => $uniqueUserId, 'ip_address' => $ipAddress);
        $format = array('%d', '%s');
        $wpdb->insert($table, $data, $format);
        return $wpdb->insert_id;
    }

    /**
     * Query Ip addresses
     *
     * @param string $ipAddress
     * @return unique_user_ip_addresses[]
     */
    public function Query_Ip_Address(string $ipAddress)
    {
        global $wpdb;
        $table = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_IP_ADDRESS_TABLE_NAME;
        return  $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE ip_address = %s", $ipAddress));
    }

    /**
     * Query User Action
     *
     * @param string $ipAddress
     * @return unique_user_action[]
     */
    public function Query_User_action(int $userId, string $actionCode)
    {
        global $wpdb;
        $table = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_ACTION_LIMIT_TABLE_NAME;
        return  $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE user_id = %d AND action_code = %s", $userId, $actionCode));
    }

    public function Insert_User_Action($userId, string $actionCode)
    {
        global $wpdb;
        $table = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_ACTION_LIMIT_TABLE_NAME;
        $data = array('user_id' => $userId, 'action_code' => $actionCode, 'action_count' => 1, 'action_date' => date('Y-m-d'));
        $format = array('%d', '%s', '%d', '%s');
        $wpdb->insert($table, $data, $format);
        return $wpdb->insert_id;
    }

    public function Update_User_action($userId, $date, $count)
    {
        Logger::debug("Update_User_Action", __FILE__);
        Logger::debug('args: ' .  implode(', ', func_get_args()));
        global $wpdb;
        $table = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_ACTION_LIMIT_TABLE_NAME;
        $query = $wpdb->prepare("UPDATE $table SET action_date = %s, action_count = %d WHERE id = %d", $date, $count, $userId);
        Logger::debug("query: $query");
        $wpdb->query($query);
    }
}
