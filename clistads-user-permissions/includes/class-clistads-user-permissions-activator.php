<?php

/**
 * Fired during plugin activation
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Permissions_Activator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		Clistads_User_Permissions_Activator::CreateDatabases();
		// Clistads_User_Permissions_Activator::runTests();
	}

	private static function CreateDatabases()
	{
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		/* unique user table */
		$table_name = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_TABLE_NAME;
		$sql = "CREATE TABLE $table_name (
			id bigint unsigned not null primary key auto_increment
		) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);

		/* unique ip address table */
		$table_name = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_IP_ADDRESS_TABLE_NAME;
		$sql = "CREATE TABLE $table_name (
			id bigint unsigned not null primary key auto_increment,
			user_id bigint unsigned not null,
			ip_address varchar(39) not null
		) $charset_collate;";
		dbDelta($sql);

		$table_name = $wpdb->prefix .CLISTADS_USER_PERMISIONS_UNIQUE_USER_ACTION_LIMIT_TABLE_NAME;
		$sql = "CREATE TABLE $table_name (
			id bigint unsigned not null primary key auto_increment,
			user_id bigint unsigned not null,
			action_code varchar(5) not null,
			action_count tinyint unsigned not null,
			action_date date not null 
		) $charset_collate;";

		dbDelta($sql);



	}

	private static function runTests(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-clistads-user-permissions-database-model.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-clistads-user-permissions-unique-user-service.php';

		$dbModel = new Clistads_User_Permissions_Database_Model();
		$uniqueUserService = new Clistads_User_Permissions_Unique_User_Service($dbModel);
		$userIp = '192.168.0.1';
		$uniqueUserService->Log_Ip_Address($userIp);
	}
}
