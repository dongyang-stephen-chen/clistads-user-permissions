<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Permissions_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		Clistads_User_Permissions_Deactivator::DeleteDatabases();
	}
	private static function DeleteDatabases(){
		global $wpdb;
		$table_name = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_TABLE_NAME;
		$sql = "DROP TABLE IF EXISTS $table_name;";
		$wpdb->query($sql);

		$table_name = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_IP_ADDRESS_TABLE_NAME;
		$sql = "DROP TABLE IF EXISTS $table_name;";
		$wpdb->query($sql);

		$table_name = $wpdb->prefix . CLISTADS_USER_PERMISIONS_UNIQUE_USER_ACTION_LIMIT_TABLE_NAME;
		$sql = "DROP TABLE IF EXISTS $table_name;";
		$wpdb->query($sql);
	}
}
