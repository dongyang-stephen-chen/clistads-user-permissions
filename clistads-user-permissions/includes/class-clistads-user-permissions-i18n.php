<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Permissions_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'clistads-user-permissions',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
