<?php

define("CLISTADS_USER_PERMISIONS_UNIQUE_USER_TABLE_NAME", 'clistads_user_permission_unique_user');
define("CLISTADS_USER_PERMISIONS_UNIQUE_USER_IP_ADDRESS_TABLE_NAME", 'clistads_user_permission_unique_user_ip_address');
define("CLISTADS_USER_PERMISIONS_UNIQUE_USER_ACTION_LIMIT_TABLE_NAME", 'clistads_user_permission_unique_user_action_limit');
define("CLISTADS_USER_PERMISIONS_REGISTER_NEW_DATING_ACCOUNT_PAGE", '/members');
define('CLISTADS_USER_PERMISIONS_DATING_PROFILE_VIEW_LIMIT', 1);