<?php

class Clistads_User_Permissions_Unique_User_Service
{
    private Clistads_User_Permissions_Database_Model $dbModel;

    public function __construct($dbModel)
    {
        $this->dbModel = $dbModel;
    }

    public function Log_Ip_Address(string $ipAddress)
    {
        Logger::debug('Log_Ip_Address', __FILE__);
        Logger::debug('args: ' .  implode(', ', func_get_args()));
        $ip_addresses = $this->dbModel->Query_Ip_Address($ipAddress);
        if (Count($ip_addresses) > 0) {
            /* ip has already been tracked */
            return $ip_addresses[0]->user_id;
        } else {
            /* new ip address */
            $newUserId = $this->dbModel->Create_New_User();
            $this->dbModel->Insert_Ip_Address($ipAddress, $newUserId);
            return $newUserId;
        }
    }

    public function Log_User_Action(int $uniqueUserId, string $actionCode)
    {
        Logger::debug('Log_User_Action', __FILE__);
        Logger::debug('args: ' .  implode(', ', func_get_args()));

        $actions = $this->dbModel->Query_User_action($uniqueUserId, $actionCode);
        if (Count($actions) > 0) {

            Logger::debug('the action exists already');

            $action = $this->dbModel->Query_User_action($uniqueUserId, $actionCode);
            if ($action[0]->action_date == date('Y-m-d')) {

                Logger::debug('the date of the last action is today');

                $this->dbModel->Update_User_action($uniqueUserId, date('Y-m-d'), $action[0]->action_count + 1);
                return $action[0]->action_count + 1;
            } else {

                Logger::debug('the date of the last action is not today');

                $this->dbModel->Update_User_action($uniqueUserId, date('Y-m-d'), 1);
                return 1;
            }
        } else {

            Logger::info("Inserting new action: $actionCode for userId: $uniqueUserId");

            $this->dbModel->Insert_User_Action($uniqueUserId, $actionCode);
            return 1;
        }
    }
}
