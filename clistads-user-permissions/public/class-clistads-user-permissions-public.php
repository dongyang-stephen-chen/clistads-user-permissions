<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Clistads_User_Permissions
 * @subpackage Clistads_User_Permissions/public
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Permissions_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private Clistads_User_Permissions_Unique_User_Service $uniqueUserService;
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$dbModel = new Clistads_User_Permissions_Database_Model();
		$this->uniqueUserService =  new Clistads_User_Permissions_Unique_User_Service($dbModel);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Clistads_User_Permissions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Clistads_User_Permissions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/clistads-user-permissions-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Clistads_User_Permissions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Clistads_User_Permissions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/clistads-user-permissions-public.js', array('jquery'), $this->version, false);
	}

	public function on_page_load()
	{
		Logger::debug('on_page_load', __FILE__);
		global $wp;
		$site_url = get_site_url();
		$current_url = home_url(add_query_arg(array(), $wp->request));
		Logger::debug("site url: $site_url");
		Logger::debug("current page url: $current_url");

		$regex = "@$site_url/(.*)@";
		Logger::debug("regular expression: $regex");
		$actionCode = null;
		if (preg_match($regex, $current_url, $matches)) {
			Logger::debug("page segment: $matches[1]");

			$url_segments = explode("/", $matches[1]);
			switch (count($url_segments)) {
				case 2:
					switch ($url_segments[0]) {
						case 'members':
							Logger::debug('viewing a dating profile');
							$actionCode = 'date';
							break;
					}
					break;
			}
		}
		$count = null;
		if ($actionCode) {
			if (!is_user_logged_in()) {
				Logger::debug('user is not logged in');
				$ip_address = $_SERVER['REMOTE_ADDR'];
				Logger::debug("ip Address: $ip_address");
				$user_id = $this->uniqueUserService->Log_Ip_Address($ip_address);
				$count = $this->uniqueUserService->Log_User_Action($user_id, $actionCode);
			} else {
				$user = wp_get_current_user();
			}
		}

		if ($count) {
			switch ($actionCode) {
				case 'date':
					if ($count > CLISTADS_USER_PERMISIONS_DATING_PROFILE_VIEW_LIMIT) {
						wp_redirect($site_url . '/' . CLISTADS_USER_PERMISIONS_REGISTER_NEW_DATING_ACCOUNT_PAGE);
					}
					break;
			}
		}
	}
}
